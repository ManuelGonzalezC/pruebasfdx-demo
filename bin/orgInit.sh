#!/bin/bash

sfdx force:source:push
sfdx force:user:permset:assign -n CPQ-QA
sfdx force:data:tree:import --plan ./data/sample-data-plan.json

echo "Org is set up"